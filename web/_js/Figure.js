var Figure = Base.extend({
	physic : null,
	graphic : null,
	joint : null,
	x : null,
	y : null,
	world : null,
	three : null,
	head : {
		width : 50
	},
	neck : {
		height: 5,
		width : 20
	},
	body : {
		width: 50,
		height: 50,
		segments : 7
	},
	arm : {
		upper: 30,
		lower : 30,
		height: 10
	},
	hand : {
		width: 20,
		height:15
	},
	leg : {
		upper: 40,
		lower : 30,
		height: 20
	},
	feet : {
		width: 15,
		height: 10
	},
	density : 5000,
	restitution : 0,
	bullet : true,
	shirtColor: null,
	categoryBits : 0x0002,
	maskBits : 0x0002,
	constructor : function(x,y,world,three){
		// Scale everything down
		var scale = 0.3;
		this.head.width *= scale;
		this.neck.height *= scale;
		this.neck.width *= scale;
		this.body.width *= scale;
		this.body.height *= scale;
		this.arm.upper *= scale;
		this.arm.lower *= scale;
		this.arm.height *= scale;
		
		this.hand.width *= scale;
		this.hand.height *= scale;
		
		this.leg.upper *= scale;
		this.leg.lower *= scale;
		this.leg.height *= scale;
		this.feet.height *= scale;
		this.feet.width *= scale;
		
		this.physic = {};
		this.joint = {};
		this.graphic = {};
		this.x = x;
		this.y = y;
		this.world = world;
		this.three = three;
		this.shirtColor = 0x945500;
		this.initHead(); // Chains to other body parts
	},
	initHead : function(){
		// Physics
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
				
		fixDef.shape = new b2CircleShape(this.p(this.head.width));
		
		
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		
		
		bodyDef.type = b2Body.b2_dynamicBody;
		bodyDef.position.Set(this.p(this.x), this.p(this.y));
		bodyDef.bullet = this.bullet;
		
		var head = this.world.CreateBody(bodyDef);
		head.CreateFixture(fixDef);
		this.physic.head = head;
				
		// Graphics
		var headTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/face.png');
		var headMaterial = new THREE.MeshBasicMaterial({
			//color : 0x999999 //0xFFFFFF * Math.random()
			map : headTexture,
			transparent : true
		});
		var headSphere = new THREE.CubeGeometry(this.head.width * 2.5, this.head.width * 3, 10);		
		var headMesh = new THREE.Mesh(headSphere, headMaterial);
		headMesh.position.z = 10;
		//headMesh.rotation.y -= Math.PI / 2;
				
		this.three.scene.add(headMesh);		
		
		this.graphic.head = headMesh;
		
		this.initNeck();
	},
	initNeck : function(){
		// Physics
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
		
		fixDef.shape = new b2PolygonShape();
		fixDef.shape.SetAsBox(this.p(this.neck.width), this.p(this.neck.height));
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		bodyDef.bullet = this.bullet;
		
		bodyDef.type = b2Body.b2_dynamicBody;
		var headPosition = this.physic.head.GetPosition();
		bodyDef.position.Set(
				headPosition.x , 
				headPosition.y + this.physic.head.m_fixtureList.m_shape.m_radius + this.p(this.neck.height)
				);

		
		var neck = this.world.CreateBody(bodyDef);
		neck.CreateFixture(fixDef);
		
		// Create joint
		var joint = this.weldJoint(
				neck, 
				this.physic.head, new b2Vec2(0,this.p(-this.neck.height)), 
				new b2Vec2(0,this.p(this.head.width))
		);
		
		this.joint.headneck = joint;

		this.physic.neck = neck;
		
		// Graphics
		var neckTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/neck.png');
		var neckMaterial = new THREE.MeshBasicMaterial({
			color : 0x333333
		});
		var neckCube = new THREE.CubeGeometry(this.neck.width * 3, this.neck.height * 3, 10);		
		var neckMesh = new THREE.Mesh(neckCube, neckMaterial);
		
		this.three.scene.add(neckMesh);				
		this.graphic.neck = neckMesh;
		
		this.initBody();
	},
	initBody : function(){
		this.physic.body = [];		
		this.graphic.body = [];
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
				
		fixDef.shape = new b2PolygonShape();		
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		bodyDef.bullet = this.bullet;
		

		
		bodyDef.type = b2Body.b2_dynamicBody;
		
		
		var body,
			lastSegment = this.physic.neck,
			lastSegmentPosition;
		
		var segmentHeight = this.body.height / this.body.segments;
		
		this.joint.body = [];
		// Graphics : Shared values for body segments
		
		for(var i = 0; i < this.body.segments; i++){
			
			var bodyMaterial = new THREE.MeshBasicMaterial({
				color : (i == this.body.segments - 1) ? 0x292d3a : this.shirtColor
			});
			
			
			fixDef.shape.SetAsBox(this.p(this.body.width), this.p(segmentHeight));
			
			lastSegmentPosition = lastSegment.GetPosition();
			
			bodyDef.position.Set(
					lastSegmentPosition.x , 
					lastSegmentPosition.y + ( lastSegment.m_fixtureList.m_shape.m_vertices[2].y ) + this.p( segmentHeight ) 
			);
			
			body = this.world.CreateBody(bodyDef);
			body.CreateFixture(fixDef);			
			
			
			if(i == 0){				
				var joint = this.weldJoint(
						body, 
						lastSegment, 
						new b2Vec2(0,-( lastSegment.m_fixtureList.m_shape.m_vertices[2].y / 2 )), 
						new b2Vec2(0,this.p( segmentHeight * 2 ))
				);	
			} else {			
				var joint = this.revoluteJoint(
						body, 
						lastSegment, 
						new b2Vec2(0,-( lastSegment.m_fixtureList.m_shape.m_vertices[2].y / 2 )), 
						new b2Vec2(0,this.p( segmentHeight * 2 )),
						0.05,
						0.0
				);	
			}
			

			
			// Push body part in to physic body array
			this.physic.body.push(body);
			this.joint.body.push(joint);
			
			// Graphics
			var bodyCube = new THREE.CubeGeometry(this.body.width * 2, segmentHeight * 4.5, 10);
			var bodyMesh = new THREE.Mesh(bodyCube, bodyMaterial);			
			this.three.scene.add(bodyMesh);				
			this.graphic.body.push(bodyMesh);
			
			lastSegment = body;
		};
		
		this.initArms();
	},
	initArms : function() {		
		
		this.joint.arms = {};
		
		var rightArm = this.createArm('right');
		var leftArm = this.createArm('left');
		
		this.physic.arms = {
				left : {
					upper : null,
					lower : null
				},
				right : {
					upper : null,
					lower : null
				}
		};			
		this.physic.arms.right = rightArm.physic;
		this.physic.arms.left = leftArm.physic;
		
		this.graphic.arms = {
				left : {
					upper : null,
					lower : null
				},
				right : {
					upper : null,
					lower : null
				}
		};
		this.graphic.arms.right = rightArm.graphic;
		this.graphic.arms.left = leftArm.graphic;
		
		this.initHands();
	},
	createArm : function(side){
		this.joint.arms[side] = {};
		
		var armObject = {
				physic : {},
				graphic : {}
		};
		var topBodySegment = this.physic.body[0];
		var topBodySegmentPosition = topBodySegment.GetPosition();
				
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
				
		fixDef.shape = new b2PolygonShape();						
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		bodyDef.bullet = this.bullet;
		
		bodyDef.type = b2Body.b2_dynamicBody;
		
		// Upper arm
		fixDef.shape.SetAsBox(this.p(this.arm.upper), this.p(this.arm.height));
		if(side == 'right'){
			bodyDef.position.Set(
					topBodySegmentPosition.x + topBodySegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.arm.upper), 
					topBodySegmentPosition.y
			);		
		} else {					
			bodyDef.position.Set(
					topBodySegmentPosition.x - topBodySegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.arm.upper), 
					topBodySegmentPosition.y
			);			
		}

		var upperArm = this.world.CreateBody(bodyDef);			
		upperArm.CreateFixture(fixDef);		
		armObject.physic.upper = upperArm;
		
		// Create joint
		if(side == 'right'){
			var jointUpper = this.revoluteJoint(
					upperArm, 
					topBodySegment, 
					new b2Vec2(-this.p(this.arm.upper),0), 
					new b2Vec2(topBodySegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.arm.height),0),
					0.5,
					0.5,
					true
			);
		} else {
			var jointUpper = this.revoluteJoint(
					upperArm, 
					topBodySegment, 
					new b2Vec2(this.p(this.arm.upper),0), 
					new b2Vec2(-topBodySegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.arm.height),0),
					0.5,
					0.5
			);
		}
		
		this.joint.arms[side].upper = jointUpper;

		// Graphics
		var armUpperTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/arm-upper.png');
		var upperArmMaterial = new THREE.MeshBasicMaterial({
			color : this.shirtColor
			//color : 0x999999
		});
		var upperArmCube = new THREE.CubeGeometry((this.arm.upper) * 3, (this.arm.height) * 2.5, 10);		
		var upperArmMesh = new THREE.Mesh(upperArmCube, upperArmMaterial);		
		this.three.scene.add(upperArmMesh);	
		armObject.graphic.upper = upperArmMesh;
		
		
		// Lower arm
		var upperArmPosition = upperArm.GetPosition();
		fixDef.shape.SetAsBox(this.p(this.arm.lower), this.p(this.arm.height));
		if(side == 'right'){
			bodyDef.position.Set(
					upperArmPosition.x + this.p(this.arm.upper) + this.p(this.arm.lower), 
					upperArmPosition.y
			);	
		} else {
			bodyDef.position.Set(
					upperArmPosition.x - this.p(this.arm.upper) - this.p(this.arm.lower), 
					upperArmPosition.y
			);			
		}
		var lowerArm = this.world.CreateBody(bodyDef);			
		lowerArm.CreateFixture(fixDef);
		
		// Create joint
		if(side == 'right'){
			var jointLower = this.revoluteJoint(
					lowerArm, 
					upperArm, 
					new b2Vec2(-this.p(this.arm.lower),0), 
					new b2Vec2(upperArm.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25,
					true
			);			
		} else {
			var jointLower = this.revoluteJoint(
					lowerArm, 
					upperArm, 
					new b2Vec2(this.p(this.arm.lower),0), 
					new b2Vec2(-upperArm.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25
			);
		}

		this.joint.arms[side].lower = jointLower;
		
		armObject.physic.lower = lowerArm;
		
		// Graphics
		var armLowerTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/arm-lower.png');
		var lowerArmMaterial = new THREE.MeshBasicMaterial({
			color : 0xbc6643
			//color : 0x999999
		});
		var lowerArmCube = new THREE.CubeGeometry(this.arm.lower * 2, this.arm.height * 2, 10);		
		var lowerArmMesh = new THREE.Mesh(lowerArmCube, lowerArmMaterial);		
		this.three.scene.add(lowerArmMesh);
		
		armObject.graphic.lower = lowerArmMesh;
		
		
		return armObject;
		
	},
	initHands : function(){
		this.joint.hands = {};
		
		this.graphic.hands = {
				left : null,
				right : null
		}
		
		this.physic.hands = {
				left : null,
				right : null
		};	
		
		var handRight = this.createHand('right');
		var handLeft = this.createHand('left');
		
		this.physic.hands.right = handRight.physic;
		this.physic.hands.left = handLeft.physic;
		
		this.graphic.hands.right = handRight.graphic;
		this.graphic.hands.left = handLeft.graphic;
			
		this.initLegs();
	},
	createHand : function(side){
		var returnObject = {
				physic : null,
				graphic : null
		}
		
		if(side == 'right'){
			var lowerArmSegment = this.physic.arms.right.lower;
		} else {
			var lowerArmSegment = this.physic.arms.left.lower;
		}
				
		var lowerArmSegmentPosition = lowerArmSegment.GetPosition();
				
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
				
		fixDef.shape = new b2PolygonShape();
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		
		bodyDef.type = b2Body.bs_dynamicBody;
		
		fixDef.shape.SetAsBox(this.p(this.hand.width), this.p(this.hand.height));	
		if(side == 'right'){
			bodyDef.position.Set(
					lowerArmSegmentPosition.x + lowerArmSegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.hand.width), 
					lowerArmSegmentPosition.y
			);
		} else {
			bodyDef.position.Set(
					lowerArmSegmentPosition.x - lowerArmSegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.hand.width), 
					lowerArmSegmentPosition.y
			);				
		}

		var hand = this.world.CreateBody(bodyDef);			
		hand.CreateFixture(fixDef);	
		hand.SetAngle(-1.5);
		
		// Create joint
		if(side == 'right'){
			var joint = this.revoluteJoint(
					hand, 
					lowerArmSegment, 
					new b2Vec2(-this.p(this.hand.width),0), 
					new b2Vec2(lowerArmSegment.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25,
					true
			);
		} else {
			var joint = this.revoluteJoint(
					hand, 
					lowerArmSegment, 
					new b2Vec2(this.p(this.hand.width),0), 
					new b2Vec2(-lowerArmSegment.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25
			);			
		}
		
		this.joint.hands[side] = joint;
		
		returnObject.physic = hand;
		
		// Graphics
		if(side == 'right')
			var handTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/hand-right.png');
		else
			var handTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/hand-left.png');
		
		var handMaterial = new THREE.MeshBasicMaterial({
			map : handTexture
		});
		var handCube = new THREE.CubeGeometry(this.hand.width * 3, this.hand.height * 3, 10);		
		var handMesh = new THREE.Mesh(handCube, handMaterial);		
		this.three.scene.add(handMesh);
		returnObject.graphic = handMesh;
		
		return returnObject;
	},
	initLegs : function(){
		this.physic.legs = {
				left : null,
				right : null
			};
	
		this.graphic.legs = {
				left : null,
				right : null
			};
		
		var legRight = this.createLeg('right');
		var legLeft = this.createLeg('left');
		
		this.physic.legs.right = legRight.physic;
		this.physic.legs.left = legLeft.physic;
		
		this.graphic.legs.right = legRight.graphic;
		this.graphic.legs.left = legLeft.graphic;
		
		this.initFeet();
	},
	createLeg : function(side){
		var legObject = {
				physic : {},
				graphic : {}
		};
		var bottomBodySegment = this.physic.body[this.physic.body.length - 1];
		var bottomBodySegmentPosition = bottomBodySegment.GetPosition();
				
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
		fixDef.restitution = this.restitution;
				
		fixDef.shape = new b2PolygonShape();	
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef();
		
		
		bodyDef.type = b2Body.b2_dynamicBody;
		
		// Upper leg
		fixDef.shape.SetAsBox(this.p(this.leg.upper), this.p(this.leg.height));
		if(side == 'right'){
			bodyDef.position.Set(
					bottomBodySegmentPosition.x + bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.leg.upper),
					bottomBodySegmentPosition.y + bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].y
			);	
		} else {					
			bodyDef.position.Set(
					bottomBodySegmentPosition.x - bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.leg.upper),
					bottomBodySegmentPosition.y + bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].y
			);				
		}

		var upperLeg = this.world.CreateBody(bodyDef);			
		upperLeg.CreateFixture(fixDef);
		legObject.physic.upper = upperLeg;
		
		
		// Create joint
		if(side == 'right'){
			this.revoluteJoint(
					upperLeg, 
					bottomBodySegment, 
					new b2Vec2(-this.p(this.leg.upper),0), 
					new b2Vec2(bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.leg.height),
							bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].y),
					0.0,
					0.75,
					true
			);
		} else {
			this.revoluteJoint(
					upperLeg, 
					bottomBodySegment, 
					new b2Vec2(this.p(this.leg.upper),0), 
					new b2Vec2(-bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.leg.height),
							bottomBodySegment.m_fixtureList.m_shape.m_vertices[2].y),
					0.75,
					0.0
			);
		}
		
		// Graphics
		var legUpperTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/leg-upper.png');
		var legUpperMaterial = new THREE.MeshBasicMaterial({
			map : legUpperTexture
			//color : 0x999999
		});
		var legUpperCube = new THREE.CubeGeometry(this.leg.upper * 2, this.leg.height * 2, 10);		
		var legUpperMesh = new THREE.Mesh(legUpperCube, legUpperMaterial);
		legUpperMesh.position.z = -1;
		
		this.three.scene.add(legUpperMesh);
		legObject.graphic.upper = legUpperMesh;
		
		// Lower leg
		var upperLegPosition = upperLeg.GetPosition();
		fixDef.shape.SetAsBox(this.p(this.leg.lower), this.p(this.leg.height));
		if(side == 'right'){
			bodyDef.position.Set(
					upperLegPosition.x + this.p(this.leg.upper) + this.p(this.leg.lower), 
					upperLegPosition.y
			);	
		} else {
			bodyDef.position.Set(
					upperLegPosition.x - this.p(this.leg.upper) - this.p(this.leg.lower), 
					upperLegPosition.y
			);			
		}
		var lowerLeg = this.world.CreateBody(bodyDef);			
		lowerLeg.CreateFixture(fixDef);
		
		
		// Create joint
		if(side == 'right'){
			this.revoluteJoint(
					lowerLeg, 
					upperLeg, 
					new b2Vec2(-this.p(this.leg.lower),0), 
					new b2Vec2(upperLeg.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25,
					true
			);			
		} else {
			this.revoluteJoint(
					lowerLeg, 
					upperLeg, 
					new b2Vec2(this.p(this.leg.lower),0), 
					new b2Vec2(-upperLeg.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.25
			);
		}
		
		
		legObject.physic.lower = lowerLeg;
		
		// Graphics
		var legLowerTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/leg-lower.png');
		var legLowerMaterial = new THREE.MeshBasicMaterial({
			map : legLowerTexture
			//color : 0x999999
		});
		var legLowerCube = new THREE.CubeGeometry(this.leg.lower * 3, this.leg.height * 2, 10);		
		var legLowerMesh = new THREE.Mesh(legLowerCube, legLowerMaterial);		
		this.three.scene.add(legLowerMesh);
		legObject.graphic.lower = legLowerMesh;
		
		return legObject;
	},
	initFeet : function(){		
		this.physic.feet = {
				left : null,
				right : null
			};	
		
		this.graphic.feet = {
				left : null,
				right : null
			};	
		
		var footRight = this.createFoot('right');
		var footLeft = this.createFoot('left');
		this.physic.feet.right = footRight.physic;
		this.physic.feet.left = footLeft.physic;
		
		this.graphic.feet.right = footRight.graphic;
		this.graphic.feet.left = footLeft.graphic;
	},
	createFoot : function(side){
		var footObject = {
				graphic : null,
				physic : null
		};
		
		if(side == 'right'){
			var lowerLegSegment = this.physic.legs.right.lower;
		} else {
			var lowerLegSegment = this.physic.legs.left.lower;
		}
				
		var lowerLegSegmentPosition = lowerLegSegment.GetPosition();
		
		console.log(lowerLegSegmentPosition);
		
		var fixDef = new b2FixtureDef;
		fixDef.density = this.density;
				
		fixDef.shape = new b2PolygonShape();	
		fixDef.filter.groupIndex = this.groupIndex;
		fixDef.filter.maskBits = this.maskBits;
		fixDef.filter.categoryBits = this.categoryBits;
		
		var bodyDef = new b2BodyDef;
		
		
		bodyDef.type = b2Body.b2_dynamicBody;
		
		fixDef.shape.SetAsBox(this.p(this.feet.width), this.p(this.feet.height));	
		if(side == 'right'){
			bodyDef.position.Set(
					lowerLegSegmentPosition.x + lowerLegSegment.m_fixtureList.m_shape.m_vertices[2].x + this.p(this.feet.width), 
					lowerLegSegmentPosition.y
			);			
		} else {
			bodyDef.position.Set(
					lowerLegSegmentPosition.x - lowerLegSegment.m_fixtureList.m_shape.m_vertices[2].x - this.p(this.feet.width), 
					lowerLegSegmentPosition.y
			);				
		}

		var foot = this.world.CreateBody(bodyDef);			
		foot.CreateFixture(fixDef);		
		
		// Create joint
		if(side == 'right'){
			this.revoluteJoint(
					foot, 
					lowerLegSegment, 
					new b2Vec2(-this.p(this.hand.width),0), 
					new b2Vec2(lowerLegSegment.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.0,
					true
			);
		} else {
			this.revoluteJoint(
					foot, 
					lowerLegSegment, 
					new b2Vec2(this.p(this.hand.width),0), 
					new b2Vec2(-lowerLegSegment.m_fixtureList.m_shape.m_vertices[2].x,0),
					0.25,
					0.0
			);			
		}
		
		footObject.physic = foot;
		
		// Graphics
		if(side == 'right')
			var footTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/foot-right.png');
		else		
			var footTexture = THREE.ImageUtils.loadTexture(stylesheetURL + '/assets/images/body/foot-left.png');
		
		
		var footMaterial = new THREE.MeshBasicMaterial({
			map : footTexture
			//color : 0x999999
		});
		var footCube = new THREE.CubeGeometry(this.feet.width * 4, this.feet.height * 4, 10);		
		var footMesh = new THREE.Mesh(footCube, footMaterial);
		footMesh.position.z = -1;
		this.three.scene.add(footMesh);
		footObject.graphic = footMesh;
		
		return footObject;
	},
	revoluteJoint : function(bodyA, bodyB, anchorA, anchorB, limitUp, limitDown, inWard) {
		var revoluteJointDef = new b2RevoluteJointDef();
		revoluteJointDef.localAnchorA.Set(anchorA.x, anchorA.y);
		revoluteJointDef.localAnchorB.Set(anchorB.x, anchorB.y);
		revoluteJointDef.bodyA = bodyA;
		revoluteJointDef.bodyB = bodyB;		
		revoluteJointDef.upperAngle  = limitUp * Math.PI;
		revoluteJointDef.lowerAngle = -limitDown * Math.PI;
		revoluteJointDef.enableLimit = true;
		revoluteJointDef.collideConnected = false;
		
		revoluteJointDef.frequencyHz = 40.0;
		revoluteJointDef.dampingRatio = 5;
		
		this.world.CreateJoint(revoluteJointDef);
		return revoluteJointDef;
	},
	weldJoint : function(bodyA, bodyB, anchorA, anchorB) {
		var weldJointDef = new b2WeldJointDef();
		weldJointDef.localAnchorA.Set(anchorA.x, anchorA.y);
		weldJointDef.localAnchorB.Set(anchorB.x, anchorB.y);
		weldJointDef.bodyA = bodyA;
		weldJointDef.bodyB = bodyB;
		return this.world.CreateJoint(weldJointDef);
	},
	resetPosition : function(){
		var self = this;
		
		this.physic.head.SetPosition(new b2Vec2(10,10));		
		this.physic.head.SetPosition(new b2Vec2(10,10));		
		this.physic.neck.SetPosition(new b2Vec2(10,10));	
		$.each(this.physic.body, function(index,value){
			value.SetPosition(new b2Vec2(10,10));
		});
				
		this.physic.hands.left.SetPosition(new b2Vec2(10,10));		
		this.physic.hands.right.SetPosition(new b2Vec2(10,10));		
		this.physic.arms.left.upper.SetPosition(new b2Vec2(10,10));		
		this.physic.arms.left.lower.SetPosition(new b2Vec2(10,10));		
		this.physic.arms.right.upper.SetPosition(new b2Vec2(10,10));		
		this.physic.arms.right.lower.SetPosition(new b2Vec2(10,10));		
		this.physic.legs.left.upper.SetPosition(new b2Vec2(10,10));			
		this.physic.legs.left.lower.SetPosition(new b2Vec2(10,10));			
		this.physic.legs.right.upper.SetPosition(new b2Vec2(10,10));			
		this.physic.legs.right.lower.SetPosition(new b2Vec2(10,10));		
		this.physic.feet.left.SetPosition(new b2Vec2(10,10));		
		this.physic.feet.right.SetPosition(new b2Vec2(10,10));
	},
	destroy : function(){
		var self = this;
		
		world.DestroyBody(this.physic.head);
		this.three.scene.remove(this.graphic.head);
		
		world.DestroyBody(this.physic.neck);
		this.three.scene.remove(this.graphic.neck);
		
		$.each(this.physic.body, function(index,value){
			world.DestroyBody(value);
		});
		
		$.each(this.graphic.body, function(index,value){
			self.three.scene.remove(value);
		});
		
		world.DestroyBody(this.physic.hands.left);
		this.three.scene.remove(this.graphic.hands.left);
		
		world.DestroyBody(this.physic.hands.right);
		this.three.scene.remove(this.graphic.hands.right);
		
		world.DestroyBody(this.physic.arms.left.upper);
		this.three.scene.remove(this.graphic.arms.left.upper);
		
		world.DestroyBody(this.physic.arms.left.lower);
		this.three.scene.remove(this.graphic.arms.left.lower);
		
		world.DestroyBody(this.physic.arms.right.upper);
		this.three.scene.remove(this.graphic.arms.right.upper);
		
		world.DestroyBody(this.physic.arms.right.lower);
		this.three.scene.remove(this.graphic.arms.right.lower);
		
		world.DestroyBody(this.physic.legs.left.upper);	
		this.three.scene.remove(this.graphic.legs.left.upper);
		
		world.DestroyBody(this.physic.legs.left.lower);	
		this.three.scene.remove(this.graphic.legs.left.lower);
		
		world.DestroyBody(this.physic.legs.right.upper);	
		this.three.scene.remove(this.graphic.legs.right.upper);
		
		world.DestroyBody(this.physic.legs.right.lower);
		this.three.scene.remove(this.graphic.legs.right.lower);
		
		world.DestroyBody(this.physic.feet.left);
		this.three.scene.remove(this.graphic.feet.left);
		
		world.DestroyBody(this.physic.feet.right);
		this.three.scene.remove(this.graphic.feet.right);
		
	},
	destroyJoints : function(){
		console.log(this.joint.headneck);
		world.DestroyJoint(this.joint.headneck);
	},
	p : function(pixels){
		return pixels / 30;
	}
});