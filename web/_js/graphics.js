function setScaleRatio(){
	global.scaleRatio = $(document).width() / global.canvasWidth;
	console.log(global.scaleRatio);
}

function initGraphics(){
	initThree();
}

function initThree(){
	$('#'+global.canvasID).width(global.canvasWidth);
	$('#'+global.canvasID).height(global.canvasHeight);
	
	var threeSettings = {
			canvas : $('#'+global.canvasID)
	};
	
	// Setup scene
	var scene = new THREE.Scene();
	threeSettings.scene = scene;
	
	//var camera = new THREE.PerspectiveCamera( 70, global.canvasWidth / global.canvasHeight, 1, 10000 );
	var camera = new THREE.OrthographicCamera( global.canvasWidth / - 2, global.canvasWidth / 2, global.canvasHeight / 2, global.canvasHeight / - 2, - 2000, 1000 );
	camera.position.set( 0, 0, 500 );
	
	scene.add(camera);
	threeSettings.camera = camera;
			
	// Setup renderer
	var renderer = new THREE.WebGLRenderer({
	});
	renderer.setSize( global.canvasWidth, global.canvasHeight );
	threeSettings.renderer = renderer;
	
	// Append canvas
	threeSettings.canvas.append( renderer.domElement );
	
	var projector = new THREE.Projector();
	threeSettings.projector = projector;
	
	global.three = threeSettings;
}