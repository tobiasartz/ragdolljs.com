var b2Vec2 = Box2D.Common.Math.b2Vec2, 
b2AABB = Box2D.Collision.b2AABB, 
b2BodyDef = Box2D.Dynamics.b2BodyDef, 
b2Body = Box2D.Dynamics.b2Body, 
b2FixtureDef = Box2D.Dynamics.b2FixtureDef, 
b2Fixture = Box2D.Dynamics.b2Fixture, 
b2World = Box2D.Dynamics.b2World, 
b2MassData = Box2D.Collision.Shapes.b2MassData, 
b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape, 
b2CircleShape = Box2D.Collision.Shapes.b2CircleShape, 
b2DebugDraw = Box2D.Dynamics.b2DebugDraw, 
b2MouseJointDef = Box2D.Dynamics.Joints.b2MouseJointDef, 
b2RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef, 
b2WeldJointDef = Box2D.Dynamics.Joints.b2WeldJointDef, 
bodyDef, 
fixDef;

var box2d = {
		obstacles : {},
		obstacleIDCounter : 0
};

function p(pixels) {
	return pixels / 30;
}

var pixel = 1 / 30;

var world;

var Wall = Base.extend({
	world : null,
	fixDef : null,
	bodyDef : null,
	body : null,
	targetPos : null,
	destroyObjects : null,
	constructor : function(type, world, x, y, width, height){
		this.world = world;
		
		var bodyDef = new b2BodyDef;
		bodyDef.type = b2Body.b2_staticBody;
		this.bodyDef = bodyDef;
		
		var fixDef = new b2FixtureDef;
		fixDef.density = 1;
		fixDef.friction = 200000;	
		fixDef.restitution = 0;	
		fixDef.shape = new b2PolygonShape;
		fixDef.shape.SetAsBox((width/2)/30, (height/2)/30);
		fixDef.filter.categoryBits = 0x0002;
		this.fixDef = fixDef;
		
		var bodyPos = new b2Vec2( (x + width/2) / 30, (y + height/2)/30 );
		this.targetPos = bodyPos;
		bodyDef.bullet = true;
		
		var body = this.world.CreateBody(bodyDef);
		body.SetPosition(bodyPos);
		body.CreateFixture(fixDef);
		this.destroyObjects = {};
		this.body = body;

		this.body.categoryBits = 0x0008;
		this.body.maskBits = 0x0008;
	},
	updatePos : function(){
		if(this.targetPos){
			var currentPos = this.body.GetPosition();
			
			if(currentPos.y < this.targetPos.y - 1 || currentPos.y > this.targetPos.y + 0.5){
				if(currentPos.y > this.targetPos.y){
					if(currentPos.y > figure.physic.head.GetPosition().y + 15){
						this.body.SetPosition(new b2Vec2(currentPos.x, currentPos.y - (1/2)));
					} else {
						this.body.SetPosition(new b2Vec2(currentPos.x, currentPos.y - (1/10)));
					}
				} else {
					this.body.SetPosition(new b2Vec2(currentPos.x, currentPos.y + (1)));					
				}		
			} else {
				this.targetPos = null;
			}
		}
	}
});

function toggleDebug(){
	if(global.debug){
		
	} else {
		global.debug = false;
	}
}

function createFloat(x,y,width,height,restitution,objectid){
	console.log('Creating obstacle');
	// Fixture values
	var fixDef = new b2FixtureDef;
	fixDef.density = 0.1;
	fixDef.restitution = restitution;	
	fixDef.shape = new b2PolygonShape;
	fixDef.filter.categoryBits = 0x0002;
	fixDef.filter.groupdIndex = -8;

	var bodyDef = new b2BodyDef;
	bodyDef.type = b2Body.b2_dynamicBody;
	
	fixDef.shape.SetAsBox((width/2)/30, (height/2)/30);
	var obstacle = world.CreateBody(bodyDef);
	obstacle.SetPosition(new b2Vec2( (x + width/2) / 30, (y + height/2)/30 ));
	obstacle.CreateFixture(fixDef);
	
	if(objectid){
		box2d.obstacles[objectid] = obstacle;
	}
	
	return obstacle;
}

function createObstacle(x,y,width,height,restitution,objectid){	
	console.log('Creating obstacle');
	// Fixture values
	var fixDef = new b2FixtureDef;
	fixDef.density = 1;
	fixDef.restitution = restitution;	
	fixDef.shape = new b2PolygonShape;
	fixDef.filter.categoryBits = 0x0002;

	var bodyDef = new b2BodyDef;
	bodyDef.type = b2Body.b2_staticBody;
	
	fixDef.shape.SetAsBox((width/2)/30, (height/2)/30);
	var obstacle = world.CreateBody(bodyDef);
	obstacle.SetPosition(new b2Vec2( (x + width/2) / 30, (y + height/2)/30 ));
	obstacle.CreateFixture(fixDef);
	
	if(objectid){
		box2d.obstacles[objectid] = obstacle;
	} else {
		box2d.obstacles[box2d.obstacleIDCounter++] = obstacle;
	}
	
	return obstacle;
}

function destroyObstacle(objectID){
	console.log('Destroying obstacle : ' + objectID);
	var obstacle = box2d.obstacles[objectID];
	if(obstacle){
		world.DestroyBody(obstacle);
		box2d.obstacles[objectID] = null;
	}
}

function initPhysics() {
	world = new b2World(new b2Vec2(0, 5) // gravity
	, true // allow sleep
	);
	
	initWalls();
	
	global.box2d.floor = new Wall('moveable', world, 0,$(window).height(),global.canvasWidth,1); // bottom
	
	// setup debug draw
	if(global.debug){
	var debugDraw = new b2DebugDraw();
	debugDraw.SetSprite(document.getElementById("physicsDebug").getContext("2d"));
	debugDraw.SetDrawScale(30.0);
	debugDraw.SetFillAlpha(0.5);
	debugDraw.SetLineThickness(1.0);
	debugDraw.SetFlags(b2DebugDraw.e_shapeBit | b2DebugDraw.e_jointBit);
	debugDraw.SetFlags(0);
	world.SetDebugDraw(debugDraw);
	}
};

function initWalls(){
	// Create the walls
	var wallTop = new Wall('regular', world, 0,0,global.canvasWidth,10); //top
	box2d.obstacles['wallTop'] = wallTop.body;
	
	var wallLeft = new Wall('regular', world, 0,-global.canvasHeight,10,global.canvasHeight*2); // left
	box2d.obstacles['wallLeft'] = wallLeft.body;
	
	var wallRight = new Wall('regular', world, global.canvasWidth - 5,-global.canvasHeight,10,global.canvasHeight*2);
	box2d.obstacles['wallRight'] = wallRight.body;
	
}


// Mouse stuff
//http://js-tut.aardon.de/js-tut/tutorial/position.html
function getElementPosition(element) {
    var elem=element, tagname="", x=0, y=0; 

    while((typeof(elem) == "object") && (typeof(elem.tagName) != "undefined")) {
       y += elem.offsetTop;
       x += elem.offsetLeft;
       tagname = elem.tagName.toUpperCase();

       if(tagname == "BODY")
          elem=0;

       if(typeof(elem) == "object") {
          if(typeof(elem.offsetParent) == "object")
             elem = elem.offsetParent;
       }
    }
    return {x: x, y: y};
 }

//mouse

var mouseX, mouseY, mousePVec, isMouseDown, selectedBody, mouseJoint;

var canvasPosition = getElementPosition(document);

document.addEventListener("mousedown", function(e) {
	isMouseDown = true;
	handleMouseMove(e);
	document.addEventListener("mousemove", handleMouseMove, true);

}, true);

document.addEventListener("mouseup", function() {
	document.removeEventListener("mousemove", handleMouseMove, true);
	isMouseDown = false;
	mouseX = undefined;
	mouseY = undefined;
}, true);

function handleMouseMove(e) {
	mouseX = (window.mouseXPos - canvasPosition.x) / 30;
	mouseY = (window.mouseYPos - canvasPosition.y) / 30;
};

function getBodyAtMouse() {
	mousePVec = new b2Vec2(mouseX, mouseY);

	var aabb = new b2AABB();
	aabb.lowerBound.Set(mouseX - 0.001, mouseY - 0.001);
	aabb.upperBound.Set(mouseX + 0.001, mouseY + 0.001);

	// Query the world for overlapping shapes.
	selectedBody = null;
	world.QueryAABB(getBodyCB, aabb);
	return selectedBody;

}

function getBodyCB(fixture) {
	if (fixture.GetBody().GetType() != b2Body.b2_staticBody) {
		if (fixture.GetShape().TestPoint(fixture.GetBody().GetTransform(),
				mousePVec)) {
			selectedBody = fixture.GetBody();
			return false;
		}
	}
	return true;
}

