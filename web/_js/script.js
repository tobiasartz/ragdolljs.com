var figure; 

$('document').ready(function(){
	global.menuPos = $('.primary-navigation').offset();
	
	$(window).keyup(function(e){
		console.log(e.keyCode);
		switch(e.keyCode){		
		case 81:
			figure.resetPosition();
			break;
		case 87:
			global.canvasWidth = $('body').innerWidth();
			global.canvasHeight = $('body').innerHeight();
			
			console.log(box2d.obstacles);
			$.each(box2d.obstacles, function(index,value){
				destroyObstacle(index);
			});
			
			initObstacles();
			initButtons();
			initWalls();
			break;
		}
	});
	
	$(document).mousemove(function(e){
		  window.mouseXPos = e.pageX;
	      window.mouseYPos = e.pageY;
	}); 
		
	$(document).on('selectstart dragstart', function(e){
		e.preventDefault();
		return false;
	});
	
	$(document).scroll(function(){
		var newPosition = new b2Vec2(global.canvasWidthHalf/30,($(window).height() + $(document).scrollTop())/30);		
		global.box2d.floor.targetPos = newPosition;				
		
		if($(document).scrollTop() > global.menuPos.top && !$('.primary-navigation-float').is(':visible')){
			$('.primary-navigation-float').slideDown(250);
		} else if($(document).scrollTop() < global.menuPos.top && $('.primary-navigation-float').is(':visible')){
			$('.primary-navigation-float').slideUp(500);
		}
		
		var documentScrollTopHalfScreen = $(document).scrollTop() + ($(window).height() / 2);
		
		$.each(global.sectionObjects, function(index,section){
			if(global.currentPageSection != section.sectionID){
				if(section.startY < documentScrollTopHalfScreen && section.endY > documentScrollTopHalfScreen){
					
					if(global.currentPageSection){
						global.sectionObjects[global.currentPageSection].onDisplayOut();
					}
					
					global.currentPageSection = section.sectionID;
					section.onDisplayIn();
					
					
				}
			}
		})
	});
		
	global.canvasWidth = $('body').innerWidth();
	global.canvasHeight = $('body').innerHeight();
	
	
	$('#physicsDebug').attr('width', global.canvasWidth);
	$('#physicsDebug').attr('height', global.canvasHeight);
	
	
	$(".fancybox").fancybox({
		maxWidth : 980
	});
	
	//initPhotos();
	//initWorkFilter();
	initPhysics();
	initGraphics();
	initApp();
	initButtons();
	animate();
	
	// Setup page objects
	global.sectionObjects['tobias-artz'] = new Section('tobias-artz');
	global.sectionObjects['work'] = new WorkSection('work');
	global.sectionObjects['contact'] = new ContactSection('contact');
});


function initPhotos(){
	$('#tobias-artz .photos li').hide();
	$('#tobias-artz .photos li').first().show();
	showNextPhoto();
}

function showNextPhoto(){
	var current = $('#tobias-artz .photos li:visible').first();
	
	var next = current.next()[0] || $('#tobias-artz .photos li:first')[0];
	
	$(next).fadeIn(6000);
	
	current.delay(6000).fadeOut(6000, function(){
		showNextPhoto();
	});
}


var stats = new Stats();
//Align top-left
stats.getDomElement().style.position = 'absolute';
stats.getDomElement().style.right = '0px';
stats.getDomElement().style.top = '0px';
document.body.appendChild( stats.getDomElement() );

function initApp(){
	figure = new Figure(global.canvasWidth / 3, 250,world,global.three);
	global.canvasWidthHalf = global.canvasWidth / 2;
	global.canvasHeightHalf = global.canvasHeight / 2;

	
	$('.primary-navigation .menu-item').each(function(index,object){
		$(object).click(function(){	
			var target = $('a', object).attr('href').replace('#/','');
			scrollTo(target);
		});
	});
	
	$('.primary-navigation-float .menu-item').each(function(index,object){
		$(object).click(function(){	
			var target = $('a', object).attr('href').replace('#/','');
			scrollTo(target);
		});
	});
	
	initObstacles();
}

function scrollTo(target){
	if($('#'+target)){
		var scrollTop = $('#' + target).offset().top - ( ($(window).height()-$('#' + target).height()) / 2);
		$.scrollTo(scrollTop + 'px', {
			duration : 2000,
			queue :false
		});
	}
}

function initObstacles(){
	$('._obstacle').each(function(index,object){
		var offset = $(object).offset();
		createObstacle(offset.left, offset.top, $(object).outerWidth(), $(object).outerHeight(), 0, $(object).attr('id'));
	});
}

function initButtons(){
	$('.primary-navigation .menu-item').each(function(index,object){
		var offset = $(object).offset();
		createObstacle(offset.left, offset.top, $(object).outerWidth(), $(object).outerHeight(), 0, $(object).attr('id'));
	});
}

function animate() {
	requestAnimationFrame( animate );
	render();
}

function render() {				
	// Mouse listeners
    if(isMouseDown && (!mouseJoint)) {
        var body = getBodyAtMouse();
        if(body) {
           var md = new b2MouseJointDef();
           md.bodyA = world.GetGroundBody();
           md.bodyB = body;
           md.target.Set(mouseX, mouseY);
           md.collideConnected = true;
           md.maxForce = 200000;
           mouseJoint = world.CreateJoint(md);
           body.SetAwake(true);
        }
     }
     
     if(mouseJoint) {
        if(isMouseDown) {
           mouseJoint.SetTarget(new b2Vec2(mouseX, mouseY));
        } else {
           world.DestroyJoint(mouseJoint);
           mouseJoint = null;
        }
     }
     
     TWEEN.update();
     
     global.box2d.floor.updatePos();
     
     if(global.box2d.floor.body.GetPosition().y < figure.physic.head.GetPosition().y){
    	 figure.resetPosition();
     }
     
     // Render body graphics
	     if(figure){
	     figure.graphic.head.position.x = (figure.physic.head.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.head.position.y = (-figure.physic.head.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.head.rotation.z = -figure.physic.head.GetAngle();
	     
	     figure.graphic.neck.position.x = (figure.physic.neck.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.neck.position.y = (-figure.physic.neck.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.neck.rotation.z = -figure.physic.neck.GetAngle();
	     
	     var bodyI = figure.physic.body.length;
	     while(bodyI--){
	         figure.graphic.body[bodyI].position.x = (figure.physic.body[bodyI].GetPosition().x * 30) - global.canvasWidthHalf;
	         figure.graphic.body[bodyI].position.y = (-figure.physic.body[bodyI].GetPosition().y * 30) + global.canvasHeightHalf;
	         figure.graphic.body[bodyI].rotation.z = -figure.physic.body[bodyI].GetAngle();
	     }
	     
	     figure.graphic.arms.left.upper.position.x = (figure.physic.arms.left.upper.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.arms.left.upper.position.y = (-figure.physic.arms.left.upper.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.arms.left.upper.rotation.z = -figure.physic.arms.left.upper.GetAngle();
	     
	     figure.graphic.arms.left.lower.position.x = (figure.physic.arms.left.lower.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.arms.left.lower.position.y = (-figure.physic.arms.left.lower.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.arms.left.lower.rotation.z = -figure.physic.arms.left.lower.GetAngle();
	     
	     figure.graphic.arms.right.upper.position.x = (figure.physic.arms.right.upper.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.arms.right.upper.position.y = (-figure.physic.arms.right.upper.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.arms.right.upper.rotation.z = -figure.physic.arms.right.upper.GetAngle();
	     
	     figure.graphic.arms.right.lower.position.x = (figure.physic.arms.right.lower.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.arms.right.lower.position.y = (-figure.physic.arms.right.lower.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.arms.right.lower.rotation.z = -figure.physic.arms.right.lower.GetAngle();
	     
	     figure.graphic.hands.right.position.x = (figure.physic.hands.right.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.hands.right.position.y = (-figure.physic.hands.right.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.hands.right.rotation.z = -figure.physic.hands.right.GetAngle();
	     
	     figure.graphic.hands.left.position.x = (figure.physic.hands.left.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.hands.left.position.y = (-figure.physic.hands.left.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.hands.left.rotation.z = -figure.physic.hands.left.GetAngle(); 
	          
	     figure.graphic.legs.right.upper.position.x = (figure.physic.legs.right.upper.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.legs.right.upper.position.y = (-figure.physic.legs.right.upper.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.legs.right.upper.rotation.z = -figure.physic.legs.right.upper.GetAngle();
	     
	     figure.graphic.legs.right.lower.position.x = (figure.physic.legs.right.lower.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.legs.right.lower.position.y = (-figure.physic.legs.right.lower.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.legs.right.lower.rotation.z = -figure.physic.legs.right.lower.GetAngle();
	     
	     figure.graphic.legs.left.upper.position.x = (figure.physic.legs.left.upper.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.legs.left.upper.position.y = (-figure.physic.legs.left.upper.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.legs.left.upper.rotation.z = -figure.physic.legs.left.upper.GetAngle();
	     
	     figure.graphic.legs.left.lower.position.x = (figure.physic.legs.left.lower.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.legs.left.lower.position.y = (-figure.physic.legs.left.lower.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.legs.left.lower.rotation.z = -figure.physic.legs.left.lower.GetAngle();
	
	     figure.graphic.feet.right.position.x = (figure.physic.feet.right.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.feet.right.position.y = (-figure.physic.feet.right.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.feet.right.rotation.z = -figure.physic.feet.right.GetAngle();
	     
	     figure.graphic.feet.left.position.x = (figure.physic.feet.left.GetPosition().x * 30) - global.canvasWidthHalf;
	     figure.graphic.feet.left.position.y = (-figure.physic.feet.left.GetPosition().y * 30) + global.canvasHeightHalf;
	     figure.graphic.feet.left.rotation.z = -figure.physic.feet.left.GetAngle();      
	}
	  
	if(global.sectionObjects[global.currentPageSection] && global.sectionObjects[global.currentPageSection].onUpdate){
		global.sectionObjects[global.currentPageSection].onUpdate();
	}     
	
	var step = 30;//stats.getFps() / 2 || 60;
	world.Step(1 / step, 20, 20);
	world.DrawDebugData();
	world.ClearForces();
		
	global.three.renderer.render( global.three.scene, global.three.camera );
	stats.update();
}