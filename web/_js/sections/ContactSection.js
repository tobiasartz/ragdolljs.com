var ContactSection = Section.extend({
	floats : null,
	init : function(){
		
		var object = $('#contact input[type=submit]');
		var offset = object.offset();
		createObstacle(offset.left, offset.top, object.outerWidth(), object.outerHeight(), 1);
	},
	onDisplayIn : function(){
		var self = this;
		this.obstacles = {};
		
		this.visible = true;
		console.log('On display in for: ' + this.sectionID);
		
		world.SetGravity( new b2Vec2( 0,0) );
		this.createFloats();
	},
	onDisplayOut : function(){
		this.visible = false;
		console.log('On display out for: ' + this.sectionID);
		world.SetGravity( new b2Vec2( 0,5) );
		
		if(this.floats){
			$.each(this.floats, function(index,float){
				world.DestroyBody(float.body);
				float.DOM.fadeOut(1000, function(){
					$(this).remove();
				});
			});
		}
		
		$('#contact img').css('opacity','1');	
	},
	createFloats : function(){
		var self = this;
		this.floats = [];
		
		$('#contact img').each(function(index,image){			
			var imageDOM = $(image).clone(false,false);
			
			imageDOM.addClass('contact-float');
			imageDOM.click(function(){
				window.open($(image).parent().attr('href'));
			});
			
			var offset = $(image).offset();
			$('body').append(imageDOM);
			self.floats.push({
				DOM : imageDOM,
				body : createFloat(offset.left, offset.top, imageDOM.width(), imageDOM.height())
			});
			
			$(image).css('opacity','0.1');			
		});
	},
	onUpdate : function(){
		if(this.floats){
			$.each(this.floats, function(index,float){
				var floatPosition = float.body.GetPosition();
				float.DOM.css('position','absolute');
				float.DOM.css('top', (floatPosition.y * 30) - (float.DOM.height() / 2));
				float.DOM.css('left', (floatPosition.x * 30) - (float.DOM.width()));
				float.DOM.rotate(float.body.GetAngle() * (180 / Math.PI));
			});
		}
	}
});