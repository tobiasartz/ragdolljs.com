var FeedSection = Section.extend({
	wordFloats : null,
	onDisplayIn : function(){
		var self = this;
		this.obstacles = {};
		
		this.visible = true;
		console.log('On display in for: ' + this.sectionID);
		
		world.SetGravity( new b2Vec2( 0,0) );
		//this.createFloats();
		//this.loadTwitterFeed();
	},
	onDisplayOut : function(){
		this.visible = false;
		console.log('On display out for: ' + this.sectionID);
		world.SetGravity( new b2Vec2( 0,5) );
		
		if(this.wordFloats){
			$.each(this.wordFloats, function(index,word){
				world.DestroyBody(word.float);
				word.DOM.fadeOut(1000, function(){
					$(this).remove();
				});
			});
		}
	},
	createFloats : function(){
		var words = $('#feed li').text().split(' ');
		var wordFloatContainer = $('#feed .float-container');
		
		var wordFloats = [];
		$.each(words, function(index,word){
			var word = word.replace(/\s+/g, '&nbsp;') + '&nbsp;';
			
			var wordDOM = $('<span class="float">');
			if(word.indexOf('~') > -1){
				wordDOM.css('clear','both');
				wordDOM.append('&nbsp;');
			} else 
				wordDOM.append(word);
				
			wordFloatContainer.append(wordDOM);
			
			var wordDOMOffset = wordDOM.offset();
			
			wordFloats.push({
				word : word,
				DOM : wordDOM,
				float : createFloat(wordDOMOffset.left, wordDOMOffset.top, wordDOM.width(), wordDOM.height())
			});
		});
		
		this.wordFloats = wordFloats;
	},
	loadTwitterFeed : function(){
		$.ajax({
			  url: '/feeds/twitter/feed.php',
			  success: $.proxy(this.onTwitterFeedLoaded, this),
			  dataType: 'json'
		});
	},
	onTwitterFeedLoaded : function(response){
		console.log(response);
		this.displayTwitter(response);
	},
	displayTwitter : function(response){
		var self = this;
		$('#twitter-feed').empty();
		
		$('#twitter-feed').append(self.createTwitterItem(response[0]));
		
		this.createFloats();
		/*
		$.each(response, function(index,tweet){
			$('#twitter-feed').append(self.createTwitterItem(tweet));
		});
		*/
	},
	createTwitterItem : function(data){
		var item = $('<li>');
		
		if(data.in_reply_to_screen_name)
			item.append('<p class="to">To: ' + data.in_reply_to_screen_name + '</p> ~ ');
		
		item.append('<p class="on">On: ' + data.created_at + '</p> ~ ');
		item.append('<p class="text">Text: ' + data.text + '</p> ~ ');
		
		return item;
	},
	onUpdate : function(){
		// Word floats
		if(this.wordFloats){
			$.each(this.wordFloats, function(index,word){
				var floatPosition = word.float.GetPosition();
				word.DOM.css('position','absolute');
				word.DOM.css('top', (floatPosition.y * 30) - (word.DOM.height() / 2));
				word.DOM.css('left', (floatPosition.x * 30) - (word.DOM.width() / 2));
				word.DOM.rotate(word.float.GetAngle() * (180 / Math.PI));
			});
		}
	}
});