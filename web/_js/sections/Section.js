var Section = Base.extend({
	sectionID : null,
	DOM : null,
	startY : null,
	endY : null,
	visible : null,
	constructor : function(sectionID){
		this.sectionID = sectionID;
		console.log('New section: ' + this.sectionID);
		this.DOM = $('#' + this.sectionID);
		this.detectY();
		this.init();
		this.onDisplayOut(); // Do display out by default
	},
	init : function(){
		// Extend
	},
	detectY : function(){
		var offset = this.DOM.offset();
		this.startY = offset.top;
		this.endY = this.startY + this.DOM.outerHeight();
		
		console.log(this.startY + ' - ' + this.endY);
	},
	onDisplayIn : function(){
		console.log('On display in for: ' + this.sectionID);
		this.visible = true;
		// extend me
	},
	onDisplayOut : function(){
		// extend me
		console.log('On display out for: ' + this.sectionID);
		this.visible = false;
	}
});