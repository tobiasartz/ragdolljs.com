var WorkSection = Section.extend({
	obstacles : null,
	filters : null,
	items : null,
	init : function(){
		var self = this;
		this.obstacles = {};
		this.filters = [];
		
		$.each(section.work.items, function(index,value){
			var DOM = $('#' + index);		
			value.DOM = DOM;
		});
		
		$('#work .work-items .item').each(function(index,value){		
			var offset = $(value).offset();
			var obstacle = createObstacle(offset.left, offset.top, $(value).outerWidth(), $(value).outerHeight());			
			
			self.obstacles[$(value).attr('id')] = {
					active : true,
					width : $(value).outerWidth(),
					height : $(value).outerHeight(),
					body : obstacle	
			};
		});
		
		this.items = section.work.items;
		this.filter();
	},
	filter : function(){
		var self = this;
		var items = $.extend(true, {}, this.items);
		
		$.each(this.filters, function(filterIndex,filter){
			$.each(items, function(itemIndex,item){
				if($.inArray(filter, item.categories) == -1){				
					delete items[itemIndex];
					item.DOM.animate({
						opacity : 0.25
					}, 500);
					$('a', item.DOM).attr('rel','');
				}			
			});
		});
		
		$('#work .filter li').removeClass('available');
		$('#work .filter li').unbind('click');
		
		$.each(items, function(index,item){
			
			$.each(item.categories, function(index,categoryName){
				$('#work #filter_' + categoryName).addClass('available');				
			});
			
			item.DOM.animate({
				opacity : 1
			},1000);
			
			$('a', item.DOM).attr('rel','work-filtered');
		});
		
		$('#work .filter li.available').click(function(e){
			e.stopPropagation();
			
			if($(e.target).is('.available')){
				if($(e.target).hasClass('active')){
					$(e.target).removeClass('active');
				} else {
					$(e.target).addClass('active');
				}
				
				var filter = $(this).attr("id").replace('filter_','');
				if($.inArray(filter, self.filters) == -1)
					self.filters.push(filter);
				else
					self.filters.splice(self.filters.indexOf(filter),1);
				
				self.filter();
			}
		});
	},
	onDisplayIn : function(){
		var self = this;
		
		this.visible = true;
		console.log('On display in for: ' + this.sectionID);
		
		$('#work .work-items .item').each(function(index,value){
			$(value).animate({
				opacity: 1
				}, 
				2000, 
				"linear"
			);
		});
	},
	onDisplayOut : function(){
		this.visible = false;
		console.log('On display out for: ' + this.sectionID);
		
		$('#work .work-items .item').each(function(index,value){
			$(value).css('opacity', 0.25);
		});
	}
});