/**
 * Created by tobias on 11/9/13.
 */
define([
    'render_lib'
],
    function () {
        var renderer = {
            settings : null,
            viewPort : null,
            viewWidth : null,
            viewHeight : null,
            _init : function(SETTINGS){
                this.settings = SETTINGS;

                this.viewPort = this.settings.viewPort || null;

                if(this.viewPort){
                    this.viewWidth = this.viewPort.clientWidth;
                    this.viewHeight = this.viewPort.clientHeight;
                } else {
                    // TODO: Exception
                }

                this.init();
            },

            init : function (){
                // override me
            },
            _createBody : function(){

                this.createBody();
            },
            createBody : function(){
                // override me
            },
            _render : function(){
                this.render();
            },
            render : function(){

            }
        };

        return renderer;
    });
