/**
 * Created by tobias on 11/9/13.
 */
define([
    'renderer'
], function (r) {
    var renderer,
        camera,
        scene,
        light;

    r.init = function () {
        // Renderer
        if (this.settings.webgl) {
            renderer = new THREE.WebGLRenderer();
        } else {
            renderer = new THREE.CanvasRenderer();
        }

        renderer.setSize(this.viewWidth, this.viewHeight);


        // Camera
        switch (this.settings.camera) {
            default:
                camera = new THREE.PerspectiveCamera(
                    45,
                    this.viewWidth / this.viewHeight,
                    0.1,
                    10000
                );
                break;
        }

        camera.position.z = 300;

        // Scene
        scene = new THREE.Scene();
        scene.add(camera);

        // Lights

        // create a point light
        var pointLight =
            new THREE.PointLight(0xFFFFFF);

// set its position
        pointLight.position.x = 10;
        pointLight.position.y = 50;
        pointLight.position.z = 130;

// add to the scene
        scene.add(pointLight);

        this.settings.viewPort.appendChild(renderer.domElement);
    };

    r.render = function () {
        renderer.render(scene, camera);
    };

    r.createBody = function (data) {
        var sphereMaterial =
            new THREE.MeshLambertMaterial(
                {
                    color: 0xCC0000
                });

        // set up the sphere vars
        var radius = 50,
            segments = 16,
            rings = 16;

// create a new mesh with
// sphere geometry - we will cover
// the sphereMaterial next!
        var sphere = new THREE.Mesh(

            new THREE.SphereGeometry(
                radius,
                segments,
                rings),

            sphereMaterial);

// add the sphere to the scene
        scene.add(sphere);
    };
});