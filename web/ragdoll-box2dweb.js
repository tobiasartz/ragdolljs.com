(function () {
    if (typeof window !== 'undefined' && this.RD) {
        var b2Vec2 = Box2D.Common.Math.b2Vec2,
            b2AABB = Box2D.Collision.b2AABB,
            b2BodyDef = Box2D.Dynamics.b2BodyDef,
            b2Body = Box2D.Dynamics.b2Body,
            b2FixtureDef = Box2D.Dynamics.b2FixtureDef,
            b2Fixture = Box2D.Dynamics.b2Fixture,
            b2World = Box2D.Dynamics.b2World,
            b2MassData = Box2D.Collision.Shapes.b2MassData,
            b2PolygonShape = Box2D.Collision.Shapes.b2PolygonShape,
            b2CircleShape = Box2D.Collision.Shapes.b2CircleShape,
            b2DebugDraw = Box2D.Dynamics.b2DebugDraw,
            b2MouseJointDef = Box2D.Dynamics.Joints.b2MouseJointDef,
            b2RevoluteJointDef = Box2D.Dynamics.Joints.b2RevoluteJointDef,
            b2WeldJointDef = Box2D.Dynamics.Joints.b2WeldJointDef;

        this.RD.physics = {
            parent : null,
            _world: null,
            settings: null,
            scale: null,
            mouseJoint: null,
            init: function (parent, settings) {
                this.parent = parent;
                this.settings = settings || {};
                this.scale = settings.scale || 1;

                var gravityX = this.settings.gravity.x || 0;
                var gravityY = this.settings.gravity.y || 0;

                this._world = new b2World(new b2Vec2(gravityX, gravityY),
                    false
                );
            },
            setGravity: function (x, y) {
                this._world.m_gravity.x = x;
                this._world.m_gravity.y = y;
            },
            createBody: function (instruction) {
                var body = this._world.CreateBody(this.createBodyDef(instruction));
                body.CreateFixture(this.createFixtureDef(instruction));

                return body;
            },
            createFixtureDef: function (instruction) {
                var fixDef = new b2FixtureDef;
                fixDef.density = instruction.density || 1.0;
                fixDef.friction = instruction.friction || 0.5;
                fixDef.restitution = instruction.restitution || 0.5;

                var shape;
                switch (instruction.shape) {
                    case RD.SHAPES.CIRCLE:
                        shape = new b2CircleShape(this.p(instruction.width * this.scale));
                        break;
                    case RD.SHAPES.SQUARE:
                    default:
                        shape = new b2PolygonShape();
                        shape.SetAsBox((instruction.width / 2) * this.scale, (instruction.height / 2) * this.scale);
                        break;
                }

                fixDef.shape = shape;

                fixDef.filter.maskBits = 0x0002;
                fixDef.filter.categoryBits = 0x0002;


                return fixDef;
            },
            createBodyDef: function (instruction) {
                var bodyDef = new b2BodyDef;

                if (instruction.static == true) {
                    bodyDef.type = b2Body.b2_staticBody;
                } else {
                    bodyDef.type = b2Body.b2_dynamicBody;
                }

                if (instruction.position) {
                    bodyDef.position.x = instruction.position.x * this.scale;
                    bodyDef.position.y = instruction.position.y * this.scale;
                }

                return bodyDef;
            },
            getPosition: function (body) {
                return {
                    x: body.GetPosition().x,
                    y: body.GetPosition().y,
                    z: 0
                };
            },
            getRotation: function (body) {
                return body.GetAngle();
            },
            setPosition: function (body, position) {
                body.position.x = position.x;
                body.position.y = position.y;
            },
            createJoint: function (bodyA, bodyB, anchorA, anchorB, limitUp, limitDown) {
                anchorA = {
                    x: anchorA.x * this.scale,
                    y: anchorA.y * this.scale
                }

                anchorB = {
                    x: anchorB.x * this.scale,
                    y: anchorB.y * this.scale
                }

                return this.createRevoluteJoint(bodyA, bodyB, anchorA, anchorB, limitUp, limitDown);
            },
            createRevoluteJoint: function (bodyA, bodyB, anchorA, anchorB, limitUp, limitDown, inWard) {
                var limitUp = limitUp || .05;
                var limitDown = limitDown || .05;
                var inWard = inWard || .005;

                var revoluteJointDef = new b2RevoluteJointDef();
                revoluteJointDef.localAnchorA.Set(anchorA.x, anchorA.y);
                revoluteJointDef.localAnchorB.Set(anchorB.x, anchorB.y);
                revoluteJointDef.bodyA = bodyA;
                revoluteJointDef.bodyB = bodyB;
                revoluteJointDef.upperAngle = limitUp * Math.PI;
                revoluteJointDef.lowerAngle = -limitDown * Math.PI;
                revoluteJointDef.enableLimit = true;
                revoluteJointDef.collideConnected = false;


                revoluteJointDef.frequencyHz = 40.0;
                revoluteJointDef.dampingRatio = 50;


                revoluteJointDef.maxMotorTorque = 100.0;


                if (inWard)
                    revoluteJointDef.motorSpeed = -0.1;
                else
                    revoluteJointDef.motorSpeed = 0.1;

                revoluteJointDef.enableMotor = true;

                var position = {
                    x: bodyB.GetPosition().x + anchorB.x - anchorA.x,
                    y: bodyB.GetPosition().y + anchorB.y - anchorA.y
                };

                bodyA.SetPosition({
                    x: position.x,
                    y: position.y
                });


                return this._world.CreateJoint(revoluteJointDef);
            },
            createWeldJoint: function (bodyA, bodyB, anchorA, anchorB) {
                var weldJointDef = new b2WeldJointDef();
                weldJointDef.localAnchorA.Set(anchorA.x, anchorA.y);
                weldJointDef.localAnchorB.Set(anchorB.x, anchorB.y);
                weldJointDef.bodyA = bodyA;
                weldJointDef.bodyB = bodyB;
                return this._world.CreateJoint(weldJointDef);
            },
            createMouseJoint: function (body, x, y) {
                x = (x - (this.parent._viewWidth / 2));
                y = -(y - (this.parent._viewHeight / 2));

                var md = new b2MouseJointDef();
                md.bodyA = this._world.GetGroundBody();
                md.bodyB = body;
                md.target.Set(x, y);
                md.collideConnected = false;
                md.maxForce = 30.0 * body.GetMass();
                this.mouseJoint = this._world.CreateJoint(md);
                body.SetAwake(true);
            },
            destroyMouseJoint: function () {
                this._world.DestroyJoint(this.mouseJoint);
                this.mouseJoint = null;
            },
            updateMouse: function (x, y) {
                x = (x - (this.parent._viewWidth / 2));
                y = -(y - (this.parent._viewHeight / 2));
                this.mouseJoint.SetTarget(new b2Vec2(x, y));
            },
            p: function (point) {
                return point / 30;
            },
            render: function () {
                this._world.Step(1 / 10, 20, 20);
                this._world.ClearForces();
            }
        };
    }
}());