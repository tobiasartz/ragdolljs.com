(function () {
    if (typeof window !== 'undefined' && this.RD) {
        this.RD.graphics = {
            _renderer: null,
            _camera: null,
            _scene: null,
            _light: null,
            _projector: null,
            _meshes: null,
            _meshDictionary: null,
            scale: 1,
            _skin: null,
            init: function (parent, settings) {
                this.settings = settings || {};
                this.scale = settings.scale || 1;
                this._meshes = [];
                this._meshDictionary = {};

                if (this.settings.webgl) {
                    this._renderer = new THREE.WebGLRenderer();
                } else {
                    this._renderer = new THREE.CanvasRenderer();
                }

                this._renderer.setSize(RD._viewWidth, RD._viewHeight);

                // Camera
                switch (this.settings.camera.type) {
                    case 'Orthographic':
                        this._camera = new THREE.OrthographicCamera(
                            RD._viewWidth / -2,
                            RD._viewWidth / 2,
                            RD._viewHeight / 2,
                            RD._viewHeight / -2,
                            1,
                            this.settings.camera.depth
                        );
                        break;
                    default:
                        this._camera = new THREE.PerspectiveCamera(
                            45,
                            RD._viewWidth / RD._viewHeight,
                            0.1,
                            this.settings.camera.depth
                        );
                        break;
                }

                this._camera.position.z = this.settings.camera.zoom;

                // Scene
                this._scene = new THREE.Scene();
                this._scene.add(this._camera);

                // create a point light
                this._light = new THREE.PointLight(0xFFFFFF);

                this._light.position.x = 10;
                this._light.position.y = 50;
                this._light.position.z = 130;

                this._scene.add(this._light);

                this._projector = new THREE.Projector();
                RD._viewDOM.appendChild(this._renderer.domElement);
            },
            createSkin: function (skinPoints) {
                this._skin = {
                    meshPoints: [],
                    faces: [],
                    mesh: null,
                    geometry: null,
                    connects : []
                };


                /*
                var meshPoints = [];
                for (var a = 0; a < skinPoints.length; a++) {
                    var point = skinPoints[a];
                    var mesh = this._meshDictionary[point.label];

                    for (var b = 0; b < point.vertices.length; b++) {
                        var index = point.vertices[b];
                        var vector = mesh.geometry.vertices[index];
                        var offset = mesh.position;

                        var match = false;
                        var newVector = vector.clone().add(offset);

                        for (var c = 0; c < meshPoints.length; c++) {
                            var test = meshPoints[c].startVector;
                            if (test.x == newVector.x && test.y == newVector.y && test.z == newVector.z) {
                                match = true;
                                break;
                            }
                        }

                        if (!match) {
                            meshPoints.push({
                                mesh: mesh,
                                vectorIndex: index,
                                vector: vector,
                                startVector: newVector,
                                duplicate: match
                            });
                        }
                    }
                }

                this._skin.meshPoints = meshPoints;

                // DRAW INITIAL SKIN
                var shape = new THREE.Shape();

                for (var i = 0; i < this._skin.meshPoints.length; i++) {
                    var item = this._skin.meshPoints[i];
                    var mesh = item.mesh;
                    var vector = mesh.geometry.vertices[item.vectorIndex].clone();
                    vector.applyMatrix4(mesh.matrixWorld);

                    if (i == 0) {
                        shape.moveTo(vector.x, vector.y);
                    } else {
                        shape.lineTo(vector.x, vector.y);
                    }
                }

                var geometry = new THREE.ShapeGeometry(shape);
                var mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0xFF0000, wireframe: true, transparent: true }));

                var scale = 1;

                mesh.position.set(0, 0, 10);
                mesh.rotation.set(0, 0, 0);
                mesh.scale.set(scale, scale, scale);

                this._skin.mesh = mesh;
                this._scene.add(mesh);
                // DRAW INITIAL SKIN


                // Dot debug
                var self = this;
                if (false) {
                    var items = sorted.splice(0);
                    setInterval(function () {
                        if (items.length > 0) {
                            var vector = items.shift();
                            var dot = self.addDot(vector.x, vector.y);
                            self._scene.add(dot);
                            self.render();
                        }
                    }, 500)
                }
                */

                //this.findConnectPoints();

                // TEST
                /*
                var mesh1 = this._meshes[0];
                var mesh2 = this._meshes[1];
                mesh2.geometry.vertices[0].set(mesh1.position.x, mesh1.position.y, mesh1.position.z).sub(mesh2.position).add(mesh1.geometry.vertices[2]);
                mesh2.geometry.dynamic = true;
                mesh2.geometry.verticesNeedUpdate = true;


                var mesh1 = this._meshes[0];
                var mesh2 = this._meshes[1];
                mesh2.geometry.vertices[5].set(mesh1.position.x, mesh1.position.y, mesh1.position.z).sub(mesh2.position).add(mesh1.geometry.vertices[7]);
                mesh2.geometry.dynamic = true;
                mesh2.geometry.verticesNeedUpdate = true;
                */

                var connectPoints = this.findConnectPoints();

                for (var key in connectPoints) {
                    var obj = connectPoints[key];
                    var TR = obj['TR'];
                    if(TR){
                        console.log(obj);
                        var mesh = TR.mesh;
                        var trackMesh = TR.trackMesh;
                        mesh.geometry.vertices[0].set(trackMesh.position.x, trackMesh.position.y, trackMesh.position.z).sub(mesh.position).add(TR.trackPoint);
                        mesh.geometry.dynamic = true;
                        mesh.geometry.verticesNeedUpdate = true;
                    }

                    var BR = obj['BR'];
                    if(BR){
                        console.log(obj);
                        var mesh = BR.mesh;
                        var trackMesh = BR.trackMesh;
                        mesh.geometry.vertices[2].set(trackMesh.position.x, trackMesh.position.y, trackMesh.position.z).sub(mesh.position).add(BR.trackPoint);
                        mesh.geometry.dynamic = true;
                        mesh.geometry.verticesNeedUpdate = true;
                    }

                    var BL = obj['BL'];
                    if(BL){
                        console.log(obj);
                        var mesh = BL.mesh;
                        var trackMesh = BL.trackMesh;
                        mesh.geometry.vertices[7].set(trackMesh.position.x, trackMesh.position.y, trackMesh.position.z).sub(mesh.position).add(BL.trackPoint);
                        mesh.geometry.dynamic = true;
                        mesh.geometry.verticesNeedUpdate = true;
                    }

                    var TL = obj['TL'];
                    if(TL){
                        console.log(obj);
                        var mesh = TL.mesh;
                        var trackMesh = TL.trackMesh;
                        mesh.geometry.vertices[5].set(trackMesh.position.x, trackMesh.position.y, trackMesh.position.z).sub(mesh.position).add(TL.trackPoint);
                        mesh.geometry.dynamic = true;
                        mesh.geometry.verticesNeedUpdate = true;
                    }
                }

            },
            findConnectPoints: function () {
                var meshLines = [];
                var connects = {};

                function lineDistance( point1, point2 )
                {
                    var xs;
                    var ys;

                    xs = point2.x - point1.x;
                    xs = xs * xs;

                    ys = point2.y - point1.y;
                    ys = ys * ys;

                    return Math.sqrt( xs + ys );
                }

                function cross(a, b, c) {
                    var crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y);
                    if(Math.abs(crossproduct) > 0){
                        return false;
                    }

                    var dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y)*(b.y - a.y);
                    if(dotproduct < 0){
                        return false;
                    }

                    var squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y);
                    if(dotproduct > squaredlengthba){
                        return false;
                    }

                    // Still here there is a match
                    var dist1 = lineDistance(a,c);
                    var dist2 = lineDistance(b,c);

                    if(dist1 < dist2){
                        return 'a';
                    } else {
                        return 'b';
                    }
                }

                function match(a, b, c) {
                    return cross(a,b,c);
                }


                function getMeshLines(mesh){
                    var lines = [];

                    for(var i = 0; i < 4; i++){
                        var pointA;
                        var pointB;
                        switch(i){
                            case 0:
                                pointA = 0;
                                pointB = 2;
                                break;
                            case 1:
                                pointA = 2;
                                pointB = 7;
                                break;
                            case 2:
                                pointA = 7;
                                pointB = 5;
                                break;
                            case 3:
                                pointA = 5;
                                pointB = 0;
                                break;
                        }

                        lines.push({
                            a: mesh.geometry.vertices[pointA].clone().add(mesh.position),
                            aIndex : pointA,
                            b: mesh.geometry.vertices[pointB].clone().add(mesh.position),
                            bIndex : pointB
                        });
                    }

                    return lines;
                }

                // create line collections

                for (var a = 0; a < this._meshes.length; a++) {
                    var mesh = this._meshes[a];
                    var lines = getMeshLines(mesh);

                    meshLines.push({
                        'mesh' : mesh,
                        'lines' : lines
                    })
                }

                for (var a = 0; a < meshLines.length; a++) {
                    var currentMesh = meshLines[a].mesh;
                    connects[currentMesh.uuid] = {};

                    for(var b = 0; b < meshLines.length; b++){
                        var matchMesh = meshLines[b].mesh;

                        if(matchMesh != currentMesh){
                            var matchLines = meshLines[b].lines;

                            for(var c = 0; c < matchLines.length; c++){
                                var matchLine = matchLines[c];
                                var TR = match(matchLine.a, matchLine.b, currentMesh.geometry.vertices[0].clone().add(currentMesh.position));

                                if(TR !== false){
                                    connects[currentMesh.uuid]['TR'] = {
                                        mesh: currentMesh,
                                        point : currentMesh.geometry.vertices[0],
                                        trackPoint : matchMesh.geometry.vertices[matchLine[TR + 'Index']],
                                        trackMesh : matchMesh
                                    }
                                }

                                var BR = match(matchLine.a, matchLine.b, currentMesh.geometry.vertices[2].clone().add(currentMesh.position));

                                if(BR !== false){
                                    connects[currentMesh.uuid]['BR'] = {
                                        mesh: currentMesh,
                                        point : currentMesh.geometry.vertices[2],
                                        trackPoint : matchMesh.geometry.vertices[matchLine[BR + 'Index']],
                                        trackMesh : matchMesh
                                    }
                                }

                                var BL = match(matchLine.a, matchLine.b, currentMesh.geometry.vertices[7].clone().add(currentMesh.position));

                                if(BL !== false){
                                    connects[currentMesh.uuid]['BL'] = {
                                        mesh: currentMesh,
                                        point : currentMesh.geometry.vertices[7],
                                        trackPoint : matchMesh.geometry.vertices[matchLine[BL + 'Index']],
                                        trackMesh : matchMesh
                                    }
                                }

                                var TL = match(matchLine.a, matchLine.b, currentMesh.geometry.vertices[5].clone().add(currentMesh.position));

                                if(TL !== false){
                                    connects[currentMesh.uuid]['TL'] = {
                                        mesh: currentMesh,
                                        point : currentMesh.geometry.vertices[5],
                                        trackPoint : matchMesh.geometry.vertices[matchLine[TL + 'Index']],
                                        trackMesh : matchMesh
                                    }
                                }
                            }
                        }
                    }
                }

                return connects;
            },
            updateSkin: function () {
                for(var i = 0; i < this._skin.connects.length; i++){

                }

                /*
                for (var i = 0; i < this._skin.meshPoints.length; i++) {
                    var item = this._skin.meshPoints[i];
                    var mesh = item.mesh;
                    var vector = mesh.geometry.vertices[item.vectorIndex].clone();
                    vector.applyMatrix4(mesh.matrixWorld);

                    if (item.duplicate) {
                        vector.x += 1;
                        vector.y += 1;
                        vector.z += 1;
                    }

                    this._skin.mesh.geometry.vertices[i].set(vector.x, vector.y, vector.z);
                }


                this._skin.mesh.geometry.dynamic = true;
                this._skin.mesh.geometry.verticesNeedUpdate = true;
                */
            },
            addDot: function (x, y) {
                var segmentCount = 10,
                    radius = 2,
                    geometry = new THREE.Geometry(),
                    material = new THREE.LineBasicMaterial({ color: 0x000000 });

                for (var i = 0; i <= segmentCount; i++) {
                    var theta = (i / segmentCount) * Math.PI * 2;
                    geometry.vertices.push(
                        new THREE.Vector3(
                            Math.cos(theta) * radius,
                            Math.sin(theta) * radius,
                            0));
                }

                var dot = new THREE.Line(geometry, material);
                dot.position.x = x;
                dot.position.y = y;
                dot.position.z = 1;

                return dot;
            },
            createBody: function (instruction) {
                var mesh;
                var geometry;
                var material = new THREE.MeshBasicMaterial(
                    {
                        color: 0xFFF000,
                        wireframe: false
                    });

                //console.log(instruction);

                switch (instruction.shape) {
                    case RD.SHAPES.SQUARE:
                        geometry = new THREE.CubeGeometry(instruction.width * this.scale, instruction.height * this.scale, 10);
                        break;
                    case RD.SHAPES.CIRCLE:
                        break;
                }

                mesh = new THREE.Mesh(geometry, material);

                this._scene.add(mesh);

                if (!instruction.wall) {
                    this._meshes.push(mesh);
                }

                if (instruction.label) {
                    if (!this._meshDictionary[instruction.label]) {
                        this._meshDictionary[instruction.label] = mesh;
                    } else {
                        console.log('Duplicate label', instruction.label);
                    }
                }

                return mesh;
            },
            drawSkin: function () {
                if (this._skin.geometry) {
                    this._scene.remove(this._skin.geometry);
                }

                var shape = new THREE.Shape();

                for (var i = 0; i < this._skin.meshPoints.length; i++) {
                    var item = this._skin.meshPoints[i];
                    var mesh = item.mesh;
                    var vector = mesh.geometry.vertices[item.vectorIndex].clone();
                    vector.applyMatrix4(mesh.matrixWorld);

                    if (i == 0) {
                        shape.moveTo(vector.x, vector.y);
                    } else {
                        shape.lineTo(vector.x, vector.y);
                    }
                }


                function addShape(shape, extrudeSettings, color, x, y, z, rx, ry, rz, s) {

                    var points = shape.createPointsGeometry();
                    var spacedPoints = shape.createSpacedPointsGeometry(100);

                    // flat shape

                    var geometry = new THREE.ShapeGeometry(shape);

                    var mesh = new THREE.Mesh(geometry, new THREE.MeshBasicMaterial({ color: 0xFF0000, wireframe: false, transparent: true }));
                    mesh.position.set(x, y, z);
                    mesh.rotation.set(rx, ry, rz);
                    mesh.scale.set(s, s, s);

                    return mesh;
                }

                if (!this._skin.geometry || true) {
                    var object = addShape(shape, null, 0xff1100, 0, 0, 10, 0, 0, 0, 1);

                    this._skin.geometry = object;
                    this._scene.add(this._skin.geometry);
                }
            },
            getBodyAtMouse: function (x, y) {
                var vector = new THREE.Vector3(( x / RD._viewWidth ) * 2 - 1, -( y / RD._viewHeight ) * 2 + 1, 0.5);

                var intersects;

                if (this.settings.camera.type == 'Orthographic') {
                    var ray = this._projector.pickingRay(vector, this._camera);
                    intersects = ray.intersectObjects(this._meshes);
                } else {
                    this._projector.unprojectVector(vector, this._camera);
                    var raycaster = new THREE.Raycaster(this._camera.position, vector.sub(this._camera.position).normalize());
                    intersects = raycaster.intersectObjects(this._meshes);
                }

                if (intersects.length > 0) {
                    intersects[ 0 ].object.material.color.setHex(Math.random() * 0xffffff);

                    return intersects[ 0 ].object;
                }

                return null;
            },
            move: function (body, position) {
                body.position.set(position.x, position.y, position.z);
            },
            rotate: function (body, rotation) {
                //body.rotation.x = rotation * Math.PI;
                //body.rotation.y = rotation * Math.PI;
                body.rotation.z = (rotation * Math.PI) / Math.PI;
            },
            render: function () {
                if (this._skin) {
                    this.updateSkin();
                }

                this._renderer.render(this._scene, this._camera);
            }
        };
    }
}());