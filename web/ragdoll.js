(function () {
    var Ragdoll = new Function();

    Ragdoll.prototype = {
        SHAPES: {
            SQUARE: 1,
            CIRCLE: 2
        },
        LOCATIONS: {
            TOP: 1,
            TOPRIGHT: 2,
            RIGHT: 3,
            BOTTOMRIGHT: 4,
            BOTTOM: 5,
            BOTTOMLEFT: 6,
            LEFT: 7,
            TOPLEFT: 8
        },
        graphics: null,
        physics: null,
        _viewDOM: null,
        _viewWidth: null,
        _viewHeight: null,
        _partsLength: null,
        _parts: null,
        _partsArray : null,
        settings : null,
        mouse : {
            down : false,
            x : null,
            y : null
        },
        currentBody : null, // Used for mouse joints
        init: function (DOM, settings) {
            this.settings = settings;
            this._setViewValues(DOM);
            this.graphics.init(this, settings);
            this.physics.init(this, settings);
            this._parseParts(settings.parts);
            this._setListeners();
            this._setLoop();
            this._render(); // Initial render to have parts positioned


            if(settings.skin){
                this.graphics.createSkin(settings.skin);
                //this.graphics.drawSkinNew();
            }

            this._render();
        },
        start: function () {
            this._onFrame();
        },
        onRender : function(){
          // Ment for extra on render events
        },
        setGravity : function(x,y){
            this.physics.setGravity(x,y);
        },
        _setViewValues: function (DOM) {
            this._viewDOM = DOM;
            this._viewWidth = this._viewDOM.clientWidth;
            this._viewHeight = this._viewDOM.clientHeight;
        },
        _setListeners : function(){
            var self = this;
            this._viewDOM.addEventListener('click', function(e){
                self._onMouseClick(e);
            }, false);
            this._viewDOM.addEventListener('mousedown', function(e){
                self._onMouseDown(e);
            }, false);
            this._viewDOM.addEventListener('mouseup', function(e){
                self._onMouseUp(e);
            }, false);
            this._viewDOM.addEventListener('mousemove', function(e){
                self._onMouseMove(e);
            }, false);
        },
        _onMouseClick : function(e){
            // Relative mouse coordinates
            this.mouse.x = e.clientX - e.target.offsetLeft;
            this.mouse.y = e.clientY - e.target.offsetTop;
        },
        _onMouseDown : function(e){
            this.mouse.x = e.clientX - e.target.offsetLeft;
            this.mouse.y = e.clientY - e.target.offsetTop;
            this.mouse.down = true;
            this.getBodyAtMouse();
        },
        _onMouseUp : function(e){
            this.mouse.x = e.clientX - e.target.offsetLeft;
            this.mouse.y = e.clientY - e.target.offsetTop;
            this.mouse.down = false;
            if(this.currentBody){
                this.currentBody = null;
                this.physics.destroyMouseJoint();
            }
        },
        _onMouseMove : function(e){
            this.mouse.x = e.clientX - e.target.offsetLeft;
            this.mouse.y = e.clientY - e.target.offsetTop;
            if(this.mouse.down){
                if(!this.currentBody){
                    this.getBodyAtMouse();
                }

                if(this.currentBody){
                    this.physics.updateMouse(this.mouse.x, this.mouse.y);
                }
            }
        },
        getBodyAtMouse : function(){
            var graphicsBody = this.graphics.getBodyAtMouse(this.mouse.x, this.mouse.y);

            if(graphicsBody){
                this.currentBody = graphicsBody;
                this.createMouseJoint(graphicsBody.RDdata.physic, this.mouse.x, this.mouse.y);
            }
        },
        createMouseJoint : function(body, x, y){
            this.physics.createMouseJoint(body, x, y);
        },
        _setLoop: function () {
            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = (function () {
                    return window.webkitRequestAnimationFrame ||
                        window.mozRequestAnimationFrame ||
                        window.oRequestAnimationFrame ||
                        window.msRequestAnimationFrame ||
                        function (callback) {
                            window.setTimeout(callback, 1000 / 60);
                        };
                })();
            }
        },
        _parseParts: function (parts) {
            var self = this;
            var finishedParts = [];
            var lastPart = null;

            if(this.settings.boxed){
                var wallWidth = 10;
                // top
                parts.push({
                    label : 'top',
                    width : this._viewWidth,
                    shape: RD.SHAPES.SQUARE,
                    height: wallWidth,
                    density : 1.0,
                    position: {
                        x : 0,
                        y : this._viewHeight / 2
                    },
                    static : true,
                    wall : true
                });

                // bottom
                parts.push({
                    label : 'bottom',
                    width : this._viewWidth,
                    shape: RD.SHAPES.SQUARE,
                    height: wallWidth,
                    density : 1.0,
                    position: {
                        x : 0,
                        y : -this._viewHeight / 2
                    },
                    static : true,
                    wall : true
                });


                // right
                parts.push({
                    label : 'right',
                    width : wallWidth,
                    density : 1.0,
                    height: this._viewHeight,
                    shape: RD.SHAPES.SQUARE,
                    position: {
                        x : this._viewWidth / 2,
                        y : 0
                    },
                    static : true,
                    wall : true
                });

                // left
                parts.push({
                    label : 'left',
                    width : wallWidth,
                    density : 1.0,
                    height: this._viewHeight,
                    shape: RD.SHAPES.SQUARE,
                    position: {
                        x : -this._viewWidth / 2,
                        y : 0
                    },
                    static : true,
                    wall : true
                });
            }

            function _parseSinglePartData(partData) {
                var graphic = self.graphics.createBody(partData);
                var physic = self.physics.createBody(partData);
                var part = {
                    graphic: graphic,
                    physic: physic,
                    data: partData
                };

                graphic.RDdata = part;
                physic.RDdata = part;

                if (partData.joint !== undefined && lastPart !== undefined) {
                    var anchorA = self._getAnchor(part, partData.joint.self);

                    if(partData.joint.target){
                        var bodyB = finishedParts[partData.joint.target];
                    } else {
                        var bodyB = lastPart;
                    }

                    var anchorB = self._getAnchor(bodyB, partData.joint.bodyB);

                    self.physics.createJoint(part.physic, bodyB.physic, anchorA, anchorB, partData.joint.limitUp, partData.joint.limitDown);
                }

                finishedParts[part.data.label] = part;

                lastPart = part;

                if (partData.children && partData.children.length > 0) {
                    var children = partData.children;
                    var length = partData.children.length;

                    for(var i = 0; i < length; i++){
                        _parseSinglePartData(children[i]);
                    }
                }
            }

            for(var i = 0; i < parts.length; i++){
                _parseSinglePartData(parts[i]);
            }

            this._parts = finishedParts;

            var partsArray = [];

            Object.keys(finishedParts).forEach(function(key) {
                partsArray.push(finishedParts[key]);
            });

            this._partsArray = partsArray;
            this._partsLength = partsArray.length;

        },
        _getAnchor: function (part, location) {
            var anchor = {
                x: 0,
                y: 0
            };

            var partWidth = part.data.width;
            var partHeight = part.data.height;

            switch (location) {
                case RD.LOCATIONS.TOP:
                    anchor.x = 0;
                    anchor.y = partHeight / 2;
                    break;
                case RD.LOCATIONS.TOPRIGHT:
                    anchor.x = partWidth / 2;
                    anchor.y = partHeight / 2;
                    break;
                case RD.LOCATIONS.RIGHT:
                    anchor.x = partWidth / 2;
                    anchor.y = 0;
                    break;
                case RD.LOCATIONS.BOTTOMRIGHT:
                    anchor.x = partWidth / 2;
                    anchor.y = -partHeight / 2;
                    break;
                case RD.LOCATIONS.BOTTOM:
                    anchor.x = 0;
                    anchor.y = -partHeight / 2;
                    break;
                case RD.LOCATIONS.BOTTOMLEFT:
                    anchor.x = -partWidth / 2;
                    anchor.y = -partHeight / 2;
                    break;
                case RD.LOCATIONS.LEFT:
                    anchor.x = -partWidth / 2;
                    anchor.y = 0;
                    break;
                case RD.LOCATIONS.TOPLEFT:
                    anchor.x = -partWidth / 2;
                    anchor.y = partHeight / 2;
                    break;
            }

            return anchor;
        },
        _onFrame: function () {
            requestAnimationFrame(RD._onFrame);
            RD._render();
        },
        _render: function () {
            var length = this._partsLength;
            var parts = this._partsArray;

            while (length--) {
                this._animateBody(parts[length]);
            }

            this.physics.render();
            this.graphics.render();

            this.onRender();
        },
        _animateBody: function (body) {
            this.graphics.move(body.graphic, this.physics.getPosition(body.physic));
            this.graphics.rotate(body.graphic, this.physics.getRotation(body.physic));
        }
    };

    if (window.RD == undefined) {
        window.RD = new Ragdoll();
    }
}());